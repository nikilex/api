<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Rules\IsTasksClosed;
use App\Sprint;
use App\Task;
use App\Services\SprintService;

class SprintController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $sprintService;

    public function __construct(SprintService $sprintService)
    {
        $this->sprintService = $sprintService;
    }

    public function index()
    {
        return Sprint::with('tasks')->get();
    }

    public function create(Request $request)
    {
        $this->sprintService->validateCreate($request);

        $sprint = Sprint::create([
            'week'  => $request->Week,
            'year'  => $request->Year,
            'state'    => 'new',
        ]);

        $json = [
            'Id' => $request->Week . '-' . substr($request->Year, -2),
        ];

        return response()->json($json, 201);
    }

    public function start(Request $request)
    {
        $this->sprintService->validateStart($request);

        $sprint       = Sprint::where([
            'week' => substr($request->sprintId, 0, 2), 
            'year' => '20'.substr($request->sprintId, -2)])
            ->update([
                'state' => 'started',
            ]);

        $json = [
            'success' => true
        ];

        return response()->json($json, 201);
    }

    public function stop(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sprint_id' => [new IsTasksClosed],
        ]);

        if ($validator->fails()) {
            $errors = [
                'Errors' => [
                    'Fields' => "",
                    'Global' => $validator->errors()->first('sprint_id'),
                ]
            ];

            // return $errors;
            return json_encode($errors, JSON_UNESCAPED_UNICODE);
        }

        $sprint = Sprint::where('id', $request->sprint_id)
            ->update([
                'state' => 'stoped',
            ]);
        return redirect('/');
    }


    public function addTask(Request $request)
    {
        $this->sprintService->validateAddTask($request);

        $sprint = Sprint::where([
            'week' => substr($request->sprintId, 0, 2), 
            'year' => '20'.substr($request->sprintId, -2)])
            ->first('id');
        $task = Task::where('id', preg_replace('/[^0-9]/', '', $request->taskId))
            ->update([
                'sprint_id' => $sprint->id,
            ]);

        $json = [
            'success' => true
        ];

        return response()->json($json, 201);
    }
}
