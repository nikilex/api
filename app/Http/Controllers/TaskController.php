<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Resources\Task as TaskResources;
use App\Services\TaskService;

class TaskController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function index(Task $task)
    {
        return new TaskResources($task);
    }

    public function addTask(Request $request)
    {
        $this->taskService->validateStore($request);

        $task = Task::create([
            'title'       => $request->Title,
            'description' => $request->Description,
        ]);

        $json = [
            'id' => 'TASK-'.$task->id
        ];

        return response()->json($json, 201);
    }

    public function estimate(Request $request)
    {
        $this->taskService->validateEstimate($request);
        
        $task = Task::where('id', preg_replace('/[^0-9]/', '', $request->id))
                    ->update([
                        'estimate' => $request->estimation,
                    ]);

        $json = [
            'success' => true
        ];

        return response()->json($json, 201);
    }

    public function close(Request $request)
    {
        $this->taskService->validateClose($request);
        
        $task = Task::where('id', preg_replace('/[^0-9]/', '', $request->taskId))
                    ->update([
                        'status' => 'closed',
        ]);

        $json = [
            'success' => true
        ];

        return response()->json($json, 201);
    }
}
