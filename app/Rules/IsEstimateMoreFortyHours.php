<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Sprint;

class IsEstimateMoreFortyHours implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sprint = Sprint::where([
            'week' => substr($value, 0, 2), 
            'year' => '20'.substr($value, -2)])
            ->with('tasks')->first();

        $summ = 0;
        foreach($sprint->tasks as $task)
        {
            $sign = preg_replace('/\d/', '', $task->estimate);
            $time = preg_replace('/[^0-9]/', '', $task->estimate);
            switch($sign) 
            {
                case 'd':
                    $summ = $summ + ($time * 24 * 60 * 60);
                break;
                case 'h':
                    $summ = $summ + ($time * 60 * 60);
                break;
                case 'mm':
                    $summ = $summ + ($time * 60);
                break;
            }
        }
        return $summ < 144000;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.moreFotyHours');
    }
}
