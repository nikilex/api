<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Sprint;

class ExistSprintId implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sprintId = Sprint::where(['week' => substr($value, 0, 2), 'year' => '20'.substr($value, -2)])->first();
        if ($sprintId)
        {
            $sprint = Sprint::find($sprintId->id);
        } else
        {
            return false;
        }

        return $sprint;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid sprintId ';
    }
}
