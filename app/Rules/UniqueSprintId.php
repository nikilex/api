<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Sprint;

class UniqueSprintId implements Rule
{
    protected $request;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sprint = Sprint::where(['week' => $this->request->Week, 'year' => $this->request->Year])->get();
       
        return $sprint->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid :attribute ';
    }
}
