<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Sprint;

class IsStarted implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = true;
        $sprints = Sprint::all();
        foreach($sprints as $sprint)
        {
            if ($sprint->state == 'started')
            {
                return false;
                break;
            }
        }

        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.isStarted');
    }
}
