<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Sprint;
use Carbon\Carbon;
use DateTime;

class FistOrLastStarted implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sprint = Sprint::where([
            'week' => substr($value, 0, 2),
            'year' => '20' . substr($value, -2)
        ])
            ->with('tasks')->first();

        $sprintDate = $this->getStartAndEndDate($sprint->week, $sprint->year);
        $mondayBeforeMondey = strtotime("-7 day", $sprintDate['week_start']);
        $sundayAfterSundey  = strtotime("+7 day", $sprintDate['week_start']);
        $result = date('U') > $mondayBeforeMondey && date('U') < $sundayAfterSundey;

        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.firstOrLastStarted');
    }

    function getStartAndEndDate($week, $year)
    {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('U');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('U');
        return $ret;
    }
}
