<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title',
        'description',
        'estimate',
        'status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function sprints()
    {
        return $this->belongsTo(Sprint::class);
    }
}
