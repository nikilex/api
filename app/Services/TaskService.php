<?php

namespace App\Services;
use Illuminate\Support\Facades\Validator;
use App\Services\ErrorService;
use Illuminate\Validation\Rule;
use App\Rules\ExistTaskId;

class TaskService extends ErrorService
{
    public function validateStore($request)
    {
        $validator = Validator::make($request->all(), [
            'Title'       => 'required|string',
            'Description' => 'required|string',
        ]);
        
        if ($validator->fails()) {
            $fields = [
                'Title'       => $validator->errors()->first('Title'),
                'Description' => $validator->errors()->first('Description')
            ];
            $validator->errors()->add('fields', $fields);

            return $this->handler($validator);
        }
    }

    public function validateEstimate($request)
    {
        $validator = Validator::make($request->all(), [
            'id' => [
                'required',
                new ExistTaskId, 
            ],
            'estimation' => 'required|string',
        ]);
        if ($validator->fails()) {
            $fields = [
                'id'         => $validator->errors()->first('id'),
                'estimation' => $validator->errors()->first('estimation')
            ];
            $validator->errors()->add('fields', $fields);

            return $this->handler($validator);
        }
    }

    public function validateClose($request)
    {
        $validator = Validator::make($request->all(), [
            'taskId' => [
                'required',
                new ExistTaskId, 
            ],
        ]);
        if ($validator->fails()) {
            $fields = [
                'taskId' => $validator->errors()->first('id'),
            ];
            $validator->errors()->add('fields', $fields);

            return $this->handler($validator);
        }
    }
}
