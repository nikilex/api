<?php

namespace App\Services;
use Illuminate\Support\Facades\Validator;
use App\Services\ErrorService;
use App\Rules\ExistTaskId;
use App\Rules\ExistSprintId;
use App\Rules\UniqueSprintId;
use App\Rules\Appreciated;
use App\Rules\IsStarted;
use App\Rules\IsEmptyTasks;
use App\Rules\FistOrLastStarted;
use App\Rules\IsEstimateMoreFortyHours;

class SprintService extends ErrorService
{
    public function validateAddTask($request)
    {
        $validator = Validator::make($request->all(), [
            'sprintId' => [
                'required',
                new ExistSprintId, 
            ],
            'taskId' => [
                'required',
                new ExistTaskId,
            ]
        ]);
        if ($validator->fails()) {
            $fields = [
                'sprintId' => $validator->errors()->first('sprintId'),
                'taskId'   => $validator->errors()->first('taskId'),
            ];
            $validator->errors()->add('fields', $fields);

            return $this->handler($validator);
        }
    }

    public function validateStart($request)
    {
        $validator = Validator::make($request->all(), [
            'sprintId' => [ 
                new IsStarted, 
                new IsEmptyTasks, 
                new Appreciated, 
                new IsEstimateMoreFortyHours, 
                new FistOrLastStarted
            ],
        ]);

        if ($validator->fails()) {

            $validator->errors()->add('global', $validator->errors()->first('sprintId'));

            return $this->handler($validator);
        }
    }

    public function validateCreate($request)
    {
        $validator = Validator::make($request->all(), [
            'Week' => [
                'required',
                'digits:2',
                new UniqueSprintId($request), 
            ],
            'Year' => [
                'required',
                'digits:4',
                new UniqueSprintId($request),
            ],
        ]);
        if ($validator->fails()) {
            $fields = [
                'Week' => $validator->errors()->first('Week'),
                'Year'   => $validator->errors()->first('Year'),
            ];

            $validator->errors()->add('fields', $fields);

            return $this->handler($validator);
        }
    }
}
