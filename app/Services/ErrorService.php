<?php

namespace App\Services;

use Illuminate\Http\Exceptions\HttpResponseException;

class ErrorService
{
    public function handler($validator)
    {
        $arrayErrorsValidator = $validator->errors()->toArray();
        $json = [
            'Errors' => [
                'Fields' => array_key_exists('fields', $arrayErrorsValidator) ? $arrayErrorsValidator['fields'] : '',
                'Global' => array_key_exists('global', $arrayErrorsValidator) ? $arrayErrorsValidator['global'] : '',
            ],
        ];
        
        // возвращаем массив ошибок
        throw new HttpResponseException(response()->json($json, 422));    
    }
}
