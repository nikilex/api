
## Установка

```
cp .env.example .env
docker-compose up -d
docker-compose exec appApi composer install
docker-compose exec appApi php artisan key:generate
docker-compose exec appApi php artisan migrate:refresh --seed
``` 