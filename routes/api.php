<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/tasks',            'TaskController@addTask') ->name('tasks');
Route::post('/estimate/task',    'TaskController@estimate')->name('estimate.task');
Route::post('/tasks/close',      'TaskController@close')   ->name('tasks.close');

Route::post('/sprints',          'SprintController@create') ->name('sprints');
Route::post('/sprints/add-task', 'SprintController@addTask')->name('sprints.add-task');
Route::post('/sprints/start',    'SprintController@start')  ->name('sprints.start');
Route::post('/sprints/stop',     'SprintController@stop')   ->name('sprints.stop');

Route::fallback(function(){
    return response()->json([
        "Errors" =>  [
          "Fields" => "",       
          "Global" =>  "Page not found"
        ]
    ], 404);
});
