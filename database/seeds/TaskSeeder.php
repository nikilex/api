<?php

use Illuminate\Database\Seeder;
use App\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Task::create([
            "title"       => "Задача",
            "description" => "Задача спринта #1",
            'estimate'    => "30mm",
            'status'      => 'opened'
        ]);
    }
}
