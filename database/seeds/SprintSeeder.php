<?php

use Illuminate\Database\Seeder;
use App\Sprint;

class SprintSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sprint::create([
            'week' => 34,
            'year' => 2020,
        ]);
    }
}
