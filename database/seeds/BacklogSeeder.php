<?php

use Illuminate\Database\Seeder;
use App\Backlog;

class BacklogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Backlog::create([
            "title" => "Беклог",
            "description" => "Беклог для добавления в него задач",
        ]);
    }
}
