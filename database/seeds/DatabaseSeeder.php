<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BacklogSeeder::class);
        $this->call(TaskSeeder::class);
        $this->call(SprintSeeder::class);
    }
}
